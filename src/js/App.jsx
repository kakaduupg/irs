import React from 'react';
import Entry from './Entry';
import Coach from './Coach';
import Rodo from './Rodo';
import Logo from './Logo';
import Team from './Team';
import Trainings from './Trainings';
import YouTube from './YouTube';
import Logos from './Logos';

export default () => (
  <>
    <Logo />
    <Entry />
    <Coach />
    <Team />
    <Trainings />
    <YouTube />
    <Logos />
    <Rodo />
  </>
);
