import React from 'react';
import { days, table } from './data/trainings';

const numberOfEvents = Math.max(...days.map((d) => table[d].length));
const events = Array.from(Array(numberOfEvents).keys());

export default () => (
  <div className="trainings table-responsive">
    <table className="table table-striped text-center">
      <thead>
        <tr>
          {days.map((d) => (<th key={`${d}`}>{d}</th>))}
        </tr>
      </thead>
      <tbody>
        {events.map((index) => (
          <tr key={`${index}`}>
            {days.map((d) => (
              <td key={`${index}-${d}`}>{table[d][index]}</td>))}
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);
