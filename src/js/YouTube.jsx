import React from 'react';
import YouTube from 'react-youtube';

const opts = {
  height: '390',
  width: '640',
};

const videos = [
  'SEQ5EuoYbjg',
  'uJPirDGjdKE',
  'OiN01w9Dyds',
  'VaQuUCqgTnA',
  'sut_mn5XRXk',
  'ibcZHeJip_Y',
].map((video) => (
  <div className="video">
    <YouTube videoId={video} opts={opts} />
  </div>
));

export default () => (
  <>
    <h2>YouTube</h2>
    <div className="youtube">
      {videos}
    </div>
  </>
);
