import React from 'react';

export default () => (
  <>
    <h2>Trener</h2>
    <div className="coach container-fluid">
      <div className="row justify-content-end">
        <div className="col-sm-12 col-md-11 col-lg-9">
          <h1 className="coach--title">Adam Wargal</h1>
          <div className="coach--text">
            <p className="coach-paragraph">
              {' '}
              Zaczynał od treningu Taekwondo w
              1993 roku na jednym z krakowski osiedli. Jako zawodnik zdobywał
              medale zawodów ogólnopolskich w Taekwondo oraz Kickboxingu.
              Posiada stopień 3 dan Taekwondo oraz 1 dan Kickboxing. Posiada
              uprawnienia instruktora dyscyplin: Taekwondo, Kickboxing, Fitness
              – ćwiczenia siłowe oraz uprawnienia trenera dyscypliny: Boks.
            </p>
            <p className="coach-paragraph">
              {' '}
              Jest prezesem i głównym trenerem w
              klubie Instytut Rozwoju Sportu. Bierze udział w przygotowaniach
              zawodników startujących w Boksie, Kickboxingu oraz Taekwondo. Jego
              podopieczni zdobywali medale Mistrzostw Polski, Mistrzostw Europy,
              Mistrzostw Świata. Absolwent Collegium Medicum Uniwersytetu
              Jagiellońskiego na kierunku Ratownictwo Medyczne.
            </p>
            <p className="coach-paragraph">
              {' '}
              Podczas studiów trenował również
              Judo w sekcji sportowej CMUJ. Organizator obozów oraz zgrupowań
              sportowych.
            </p>
          </div>
        </div>
      </div>
    </div>
  </>
);
