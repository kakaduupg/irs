import React from 'react';

export default () => (
  <div className="logo">
    <div className="icons">
      <a
        href="https://www.youtube.com/channel/UCu0dFoZJMJmgubPZqP0Q6Qw/videos"
        target="_blank"
        rel="noreferrer"
      >
        <i className="fab fa-2x fa-youtube" />
      </a>
      <a 
        href="https://www.facebook.com/IRSsportywalki"
        rel="noreferrer"
        target="_blank"
      >
        <i
          className="fab fa-2x fa-facebook"
        />
      </a>
    </div>
  </div>
);
