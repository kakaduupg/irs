import React from 'react';
import '../assets/logos/fit.jpg';
import '../assets/logos/ck-system.png';
import '../assets/logos/benefit-systems.jpg';

const logos = [
  'benefit-systems.jpg',
  'ck-system.png',
  'fit.jpg',
].map((logo) => (
  <img src={`assets/${logo}`} alt="logo" key={logo} />
));

export default () => (
  <>
    <h2>Współpracujemy z</h2>
    <div className="logos">
      {logos}
    </div>
  </>
);
