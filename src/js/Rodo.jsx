import React from 'react';

export default () => (
  <div className="rodo container-fluid">
    <div className="row justify-content-center">
      <div className="col-sm-8">
        <h3 className="rodo--title">Ochrona danych osobowych</h3>
        <p>Administratorami danych osobowych są</p>
        <ul>
          <li>
            Stowarzyszenie Instytut Rozwoju Sportu z siedzibą w Krakowie ul.
            Łużycka 14.
          </li>
          <li>
            Przedsiębiorstwo Wargalsport z siedzibą w Krakowie ul. Łużycka
            14.
          </li>
        </ul>
        <p>
          Przetwarzanie danych osobowych odbywa się na zasadach
          współadministracji (art. 26 RODO)
        </p>

        <p>Cel przetwarzania danych:</p>
        <ul>
          <li>
            prowadzenie zajęć sportowych, w szczególności treningów boksu,
            kickboxingu, taekwando, fight cross,
          </li>
          <li>udział w zawodach sportowych</li>
          <li>członkostwo w Stowarzyszeniu Instytut Rozwoju Sportu</li>
        </ul>

        <p>Podstawa przetwarzania danych:</p>
        <p>
          Art. 6 ust. 1 lit b) RODO – wykonanie umowy, której stroną jest
          osoba, której dane dotyczą.
        </p>

        <p>Dane osobowe nie są przekazywane odbiorcom.</p>

        <p>
          Dane będą przechowywane przez okres trwania umowy oraz po jej
          zakończeniu przez czas niezbędny do zabezpieczenia dochodzenia
          roszczeń.
        </p>

        <p>Osoba, której dane dotyczą, ma prawo:</p>
        <ul>
          <li>dostępu do dotyczących jej danych,</li>
          <li>sprostowania dotyczących jej danych,</li>
          <li>usunięcia lub ograniczenia przetwarzania danych,</li>
          <li>wniesienia sprzeciwu wobec przetwarzania.</li>
          <li>wniesienia skargi do organu nadzorczego</li>
        </ul>

        <p>
          Podanie danych osobowych jest dobrowolne, jednak warunkuje udział w
          zajęciach sportowych prowadzonych przez IRS i Wargalsport.
        </p>
      </div>
    </div>
  </div>
);
