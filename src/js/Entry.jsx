import React from 'react';
import '../assets/intro.mp4';

export default () => (
  <div className="entry container-fluid">
    <video autoPlay muted loop className="entry--video">
      <source src="assets/intro.mp4" type="video/mp4" />
    </video>
    <div className="entry--text">
      <p>Treningi personalne i grupowe.</p>
      <p>
        Zadzwoń:
        <a href="tel:535 673 205" className="entry--phone">535 673 205</a>
      </p>
      <p className="entry--email">
        Napisz:
        <a href="mailto:instytutrozwojusportu@gmail.com?subject=Zapytanie o trening">instytutrozwojusportu@gmail.com</a>
      </p>
    </div>
  </div>
);
